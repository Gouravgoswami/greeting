'use Strict';

var greetingApp = greetingApp || {};

greetingApp.controllers = angular.module('greetingControllers',['ui.bootstrap','ngMaterial']);


greetingApp.controllers.controller('RootCtrl', function($scope, $location, oauth2Provider){
	
	    $scope.isActive = function (viewLocation) {
	        return viewLocation === $location.path();
	    };

	    /**
	     * Returns the OAuth2 signedIn state.
	     *
	     * @returns {oauth2Provider.signedIn|*} true if siendIn, false otherwise.
	     */
	    $scope.getSignedInState = function () {
	        return oauth2Provider.signedIn;
	    };

	    /**
	     * Calls the OAuth2 authentication method.
	     */
	    $scope.signIn = function () {
	        oauth2Provider.signIn(function () {
	        	
	        	
	            gapi.client.oauth2.userinfo.get().execute(function (resp) {
	                $scope.$apply(function () {
	                    if (resp.email) {
	                        oauth2Provider.signedIn = true;
	                        $scope.alertStatus = 'success';
	                        $scope.rootMessages =  resp.email;
	                    }
	                });
	            });
	        });
	    };
	    
	    /**
	     * Render the signInButton and restore the credential if it's stored in the cookie.
	     * (Just calling this to restore the credential from the stored cookie. So hiding the signInButton immediately
	     *  after the rendering)
	     */
	    $scope.initSignInButton = function () {
	        gapi.signin.render('signInButton', {
	            'callback': function () {
	                jQuery('#signInButton button').attr('disabled', 'true').css('cursor', 'default');
	                if (gapi.auth.getToken() && gapi.auth.getToken().access_token) {
	                    $scope.$apply(function () {
	                        oauth2Provider.signedIn = true;
	                    });
	                }
	            },
	            'clientid': oauth2Provider.CLIENT_ID,
	            'cookiepolicy': 'single_host_origin',
	            'scope': oauth2Provider.SCOPES
	        });
	    };

	    /**
	     * Logs out the user.
	     */
	    $scope.signOut = function () {
	        oauth2Provider.signOut();
	        $scope.alertStatus = 'success';
	        $scope.rootMessages = 'Logged out';
	    };

});